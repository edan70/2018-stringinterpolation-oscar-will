# conrad, the extendJ string interpolator

---

conrad extends extendJ to support string interpolation. This project is an extension of ExtendJ, an extensible java compiler built using JastAdd. String interpolation is a feature meant to make writing complex strings easier and more readable and is already available in other languages such as bash. It allows the programmer to embedd expressions directly into strings. Currently there is no support for nested string interpolations or the $varname shortform.

## installation

---

Clone the repository recursively, which will download ExtendJ in addition to our code with the following command.
`git clone --recursive https://bitbucket.org/edan70/2018-stringinterpolation-oscar-will/`

## build, test && run

---

On unix with Gradle installed you can run:
`./gradlew jar`
which will run build conrad as a jar. Testing is similar, run:

`./gradlew test`

Use conrad to compile a file (in this case BasicDemo.java in the folder examples) using built jar file, and then run the file with java, run:

`java -jar compiler.jar examples/BasicDemo.java`
`java -cp examples/ BasicDemo`

## examples

---

String interpolation enables expressions in strings, just like this:
`"Hello, I am ${age} years old, and my favorite number right now is ${Math.random() * 100}"`

Some small code examples can be found in the examples directory, the tests can also be helpful in understanding limits of the concept. These can be run using the commands in the previous section.

## project structure

---

Notable files in this project:

* build.gradle - main build script. Modified to include the parser changes.
* gradlew.bat - build script wrapper for Windows.
* gradlew - build script wrapper for for unix-based systems.
* testfiles/ - contains automated test files.
* examples/ - contains some small code examples files.
* settings.gradle - contains the build settings for the project.
* conrad.jar - the generated Jar file for the conrad extension.
* extendj/ - contains the ExtendJ compiler.
* src/jastadd/ - contains the JastAdd source code which extends the string literal class.
* src/parser/ - contains the Beaver source code which is used to add the expression goal for the parser.

## credits

---

This project was based on the [compiler template project from ExtendJ](https://bitbucket.org/extendj/compiler-template/src/master/), (licensed under BSD 2-clause) the files that have been changed are listed bellow.

* testfiles/
* examples/
* src/jastadd/
* src/parser/
* build.gradle
* settings.gradle
* README.md

## license

---

This repository is covered by the license BSD 2-clause, see file LICENSE.

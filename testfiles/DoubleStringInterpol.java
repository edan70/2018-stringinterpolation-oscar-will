public class DoubleStringInterpol{
    public static void main(String[] args){
        String a = "is ";
        String b = "an";
        System.out.println("This ${a + b} interpolation");
    }
}

/*EXPECTED
This is an interpolation
*/

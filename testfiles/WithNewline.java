public class WithNewline{
    public static void main(String[] args){
        String nl = "second,\nand finally the";
        System.out.println("This is the first line,\nthis is the ${nl} third.");
    }
}

/*EXPECTED
This is the first line,
this is the second,
and finally the third.
*/

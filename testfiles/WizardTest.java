public class WizardTest {
    public static void main(String[] args){
       String name = "Harry";
       String profession = "wizard"; 
       System.out.println("You're a ${profession}, ${name}");
    }
}

/*EXPECTED
You're a wizard, Harry
*/

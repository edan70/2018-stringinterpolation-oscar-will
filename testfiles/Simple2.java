public class Simple2{
    public static void main(String[] args){
        String s1 = "S";
        String s2 = "i";
        String s3 = "m";
        String s4 = "p";
        String s5 = "l";
        String s6 = "e";
        String s7 = "!";
        System.out.println("${s1}${s2}${s3}${s4}${s5}${s6}${s7}");
    }
}

/*EXPECTED
Simple!
*/

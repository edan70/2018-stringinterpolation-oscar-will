public class WeirdChars {
  public static void main(String[] args) {
    String name = "lasse";
    System.out.println("${name},,!,!,!?? marks the spot");
  }
}

/*EXPECT
lasse,,!,!,!?? marks the spot
*/

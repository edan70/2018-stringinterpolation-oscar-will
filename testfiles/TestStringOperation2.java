public class TestStringOperation2{
    public static void main(String[] args){
        String s1 = "This is";
        String s2 = "a bunch of";
        String s3 = "interpolated strings";
        String s4 = "${s1} ${s2} ${s3}";
        System.out.println(s4);
        int length = s4.length();
        System.out.println("the length is ${length}");
    }
}

/*EXPECTED
This is a bunch of interpolated strings
the length is 39
*/

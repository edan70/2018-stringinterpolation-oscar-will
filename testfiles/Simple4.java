public class Simple4{
    public static void main(String[] args){
        String s1 = "Hello";
        String s2 = " World";
        System.out.println("Well ${s1+s2}!");
    }
}

/*EXPECTED
Well Hello World!
*/

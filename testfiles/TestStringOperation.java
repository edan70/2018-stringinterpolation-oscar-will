public class TestStringOperation{
    public static void main(String[] args){
        String s1 = "this entire string is an interpolation";
        String s2 = "${s1}";
        System.out.println(s2);
        System.out.print("And this is the length of the string: ");
        System.out.println(s2.length());
    }
}

/*EXPECTED
this entire string is an interpolation
And this is the length of the string: 38
*/

public class Concatenation{
    public static void main(String[] args){
        String s1 = "and";
        String s2 = "this";
        String s3 = "${s2} is a concatenation ";
        String s4 = "${s1} an interpolation at once";
        System.out.println(s3+s4);
    }
}

/*EXPECTED
this is a concatenation and an interpolation at once
*/

public class EndOfString {
  public static void main(String[] args) {
      String thing = "string";
    System.out.println("interpolate at the end of the ${thing}");
  }
}

/*EXPECT
interpolate at the end of the string
*/

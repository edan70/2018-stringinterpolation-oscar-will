public class BeginningAndEnd{
    public static void main(String[] args){
        String first = "dusk";
        String last = "dawn";
        System.out.println("${first} til ${last}");
    }
}

/*EXPECTED
dusk til dawn
*/

public class Simple3{
    public static void main(String[] args){
        String s1 = "e!";
        String s2 = "pl${s1}";
        String s3 = "im${s2}";
        System.out.println("S${s3}");
    }
}

/*EXPECTED
Simple!
*/

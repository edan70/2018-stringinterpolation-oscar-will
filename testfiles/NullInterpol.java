public class NullInterpol {
  public static void main(String[] args) {
    String name = null;
    System.out.println("${name} marks the spot");
  }
}

/*EXPECT
null marks the spot
*/

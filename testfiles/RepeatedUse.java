public class RepeatedUse{
    public static void main(String[] args){
        String s1 = "ashes";
        String s2 = "dust";
        System.out.println("${s1} to ${s1}, ${s2} to ${s2}");
    }
}

/*EXPECTED
ashes to ashes, dust to dust
*/

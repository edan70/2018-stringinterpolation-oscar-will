import java.util.ArrayList;
import java.util.Random;

public class FuncCall{
    private static int genInt() { return 42; }
    public static void main(String[] args){
        System.out.println("${genInt()}");
    }
}

/*EXPECT
42
*/
